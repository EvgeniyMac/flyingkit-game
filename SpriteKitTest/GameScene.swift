//
//  GameScene.swift
//  SpriteKitTest
//
//  Created by Evgeniy Suprun on 18.03.2018.
//  Copyright © 2018 Evgeniy Suprun. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // Animations
    var animations = AnimationCoinClass()

    // Variables
    var sound = true
    var gameViewControllerBridge: GameViewController!
    var moveElectricGateY = SKAction()
    var shieldBool = false
    var score = 0
    var highScore = 0
    var gameover = 0
    
    // Texture
    
    var bgTexture: SKTexture!
    var flyHeroTex: SKTexture!
    var runHeroTex: SKTexture!
    var coinTexture: SKTexture!
    var redCoinTexture: SKTexture!
    var coinHeroTex: SKTexture!
    var redCoinHeroTex: SKTexture!
    var electricGateTex: SKTexture!
    var deadHeroTex: SKTexture!
    var shieldTexture: SKTexture!
    var shieldItemTexture: SKTexture!
    var mineTexture1: SKTexture!
    var mineTexture2: SKTexture!
    
    // Emiters node
    
    var heroEmiter1 = SKEmitterNode()
    
    // Label Nodes
    var tabToPlayLabel = SKLabelNode()
    var scoreLabel = SKLabelNode()
    var highScoreLabel = SKLabelNode()
    var highScoreTextLabel = SKLabelNode()
    var stageLabel = SKLabelNode()
    
    // Sprite Nodes
    
    var bg = SKSpriteNode()
    var ground = SKSpriteNode()
    var sky = SKSpriteNode()
    var hero = SKSpriteNode()
    var coin = SKSpriteNode()
    var redCoin = SKSpriteNode()
    var electricGate = SKSpriteNode()
    var shield = SKSpriteNode()
    var shieldItem = SKSpriteNode()
    var mine = SKSpriteNode()
    var labelObject = SKSpriteNode()

    
    // Sprite object
    
    var bgObject = SKNode()
    var groundObject = SKNode()
    var movingObject = SKNode()
    var heroObject = SKNode()
    var heroEmiterObject = SKNode()
    var coinObject = SKNode()
    var redCoinObject = SKNode()
    var shieldObject = SKNode()
    var shieldItemObject = SKNode()
    
    // Create Bit Mask
    var heroGroup: UInt32 = 0x1 << 1
    var groundGroup: UInt32 = 0x1 << 2
    var coinGroup: UInt32 = 0x1 << 3
    var redCoinGroup: UInt32 = 0x1 << 4
    var objectGroup: UInt32 = 0x1 << 5
    var shieldGroup: UInt32 = 0x1 << 6
    
    // Textures array for animate with texture
    var heroFlyTexturesArray = [SKTexture]()
    var heroRunTexturesArray = [SKTexture]()
    var coinTexturesArray = [SKTexture]()
    var electricGateTexsturesArray = [SKTexture]()
    var heroDeathTextureArray = [SKTexture]()
    
    // Timers
    var timerAddCoin = Timer()
    var timerAddRedCoin = Timer()
    var timerElectricGate = Timer()
    var timerAddShieldItem = Timer()
    var timerAddMine = Timer()
    
    
    // Sounds
    var pickCoinPreload = SKAction()
    var electricGateCreate = SKAction()
    var electricHeroDead = SKAction()
    var shieldOnPreload = SKAction()
    var shieldOffPreload = SKAction()
    
 
    override func didMove(to view: SKView) {
        
        // Background Texture
        bgTexture = SKTexture(imageNamed: "bg01.png")
        
        // Hero texture
        
        flyHeroTex = SKTexture(imageNamed: "Fly0.png")
        runHeroTex = SKTexture(imageNamed: "Run0.png")
        
        // Coin texture
        
        coinTexture = SKTexture(imageNamed: "coin.jpg")
        redCoinTexture = SKTexture(imageNamed: "coin.jpg")
        coinHeroTex = SKTexture(imageNamed: "Сoin0.png")
        redCoinHeroTex = SKTexture(imageNamed: "Сoin0.png")
        
        // Electric Gate Texture
        
        electricGateTex = SKTexture(imageNamed: "ElectricGate01.png")
        
        // Shields texture
        shieldTexture = SKTexture(imageNamed: "shield.png")
        shieldItemTexture = SKTexture(imageNamed: "shieldItem.png")
        
        // Mines texture
        mineTexture1 = SKTexture(imageNamed: "mine1.png")
        mineTexture2 = SKTexture(imageNamed: "mine2.png")
        
        // Emitters
        
        heroEmiter1 = SKEmitterNode(fileNamed: "engine.sks")!
        
        self.physicsWorld.contactDelegate = self
        
        createObjects()
        
        if UserDefaults.standard.object(forKey: "highScore") != nil {
            highScore = UserDefaults.standard.object(forKey: "highScore") as! Int
        }
        if gameover == 0 {
             createGame()
        }

        pickCoinPreload = SKAction.playSoundFileNamed("pickCoin.mp3", waitForCompletion: false)
        electricGateCreate = SKAction.playSoundFileNamed("electricCreate.wav", waitForCompletion: false)
        electricHeroDead = SKAction.playSoundFileNamed("electricDead.mp3", waitForCompletion: false)
        shieldOnPreload = SKAction.playSoundFileNamed("shieldOn.mp3", waitForCompletion: false)
        shieldOffPreload = SKAction.playSoundFileNamed("shieldOff.mp3", waitForCompletion: false)
   }
   
    func createObjects() {
        self.addChild(bgObject)
        self.addChild(groundObject)
        self.addChild(movingObject)
        self.addChild(heroObject)
        self.addChild(heroEmiterObject)
        self.addChild(coinObject)
        self.addChild(redCoinObject)
        self.addChild(shieldObject)
        self.addChild(shieldItemObject)
        self.addChild(labelObject)
        
    }
    
    func createGame() {
        createBg()
        createGround()
        createSky()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.createHero()
            self.createHeroEmitter()
            self.timerfunc()
            self.addElectricGate()
        }
        
        showTapToPlay()
        showScore()
        showStage()
        highScoreTextLabel.isHidden = true
        
        
        gameViewControllerBridge.reloadGameBtn.isHidden = true
        
        if labelObject.children.count != 0 {
            labelObject.removeAllChildren()
        }
    }
    
    func createBg() {
        bgTexture = SKTexture(imageNamed: "bg01.png")
        
        let moveBg = SKAction.moveBy(x: -bgTexture.size().width, y: 0, duration: 3)
        let replaceBg = SKAction.moveBy(x: bgTexture.size().width, y: 0, duration: 0)
        let moveBgForever = SKAction.repeatForever(SKAction.sequence([moveBg, replaceBg]))
        
        for i in 0..<3 {
            bg = SKSpriteNode(texture: bgTexture)
            bg.position = CGPoint(x: size.width/4 + bgTexture.size().width * CGFloat(i), y: size.height/2.0)
            bg.size.height = self.frame.height
            bg.run(moveBgForever)
            bg.zPosition = -1
            
            bgObject.addChild(bg)
        }
    }
    func createGround() {
        ground = SKSpriteNode()
        ground.position = CGPoint.zero
        ground.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.frame.width, height: self.frame.height/4 + self.frame.height/8))
        ground.physicsBody?.isDynamic = false
        ground.physicsBody?.categoryBitMask = groundGroup
        ground.zPosition = 1
        
        groundObject.addChild(ground)
        
    }
    
    func createSky() {
        sky = SKSpriteNode()
        sky.position = CGPoint(x: 0, y: self.frame.maxX)
        sky.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.frame.size.width + 600, height: self.frame.height - 100))
        sky.physicsBody?.isDynamic = false
        sky.zPosition = 1
        
        movingObject.addChild(sky)
        
    }
    func addHero(heroNode: SKSpriteNode, atPosition position: CGPoint) {
        hero = SKSpriteNode(texture: flyHeroTex)
        
        // Animation hero
        
        heroFlyTexturesArray = [SKTexture(imageNamed: "Fly0.png"), SKTexture(imageNamed: "Fly1.png"), SKTexture(imageNamed: "Fly2.png"), SKTexture(imageNamed: "Fly3.png"), SKTexture(imageNamed: "Fly4.png")]
        let heroFlyAnimation = SKAction.animate(with: heroFlyTexturesArray, timePerFrame: 0.1)
        let flyHero = SKAction.repeatForever(heroFlyAnimation)
        hero.run(flyHero)
        
        hero.position = position
        hero.size.height = 84
        hero.size.width = 120
        
        hero.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: hero.size.width - 40, height: hero.size.height - 30))
        hero.physicsBody?.categoryBitMask = heroGroup
        hero.physicsBody?.contactTestBitMask = groundGroup | coinGroup | redCoinGroup | objectGroup | shieldGroup
        hero.physicsBody?.collisionBitMask = groundGroup
        
        hero.physicsBody?.isDynamic = true
        hero.physicsBody?.allowsRotation = false
        hero.zPosition = 1
        
        heroObject.addChild(hero)
        
    }
    
    func createHero() {
        
        addHero(heroNode: hero, atPosition: CGPoint(x: self.size.width/4, y: 0 + flyHeroTex.size().height + 200))
        
    }
    
    func createHeroEmitter() {
        heroEmiter1 = SKEmitterNode(fileNamed: "engine.sks")!
        heroEmiterObject.zPosition = 1
        heroEmiterObject.addChild(heroEmiter1)
    }
    
    @objc func addCoin() {
        coin = SKSpriteNode(texture: coinTexture)
        
        coinTexturesArray = [SKTexture(imageNamed: "Coin0.png"), SKTexture(imageNamed: "Coin1.png"), SKTexture(imageNamed: "Coin2.png"), SKTexture(imageNamed: "Coin3.png")]
        let coinAnimation = SKAction.animate(with: coinTexturesArray, timePerFrame: 0.3)
        let coinHero = SKAction.repeatForever(coinAnimation)
        coin.run(coinHero)
        
        let monementAmount = arc4random() % UInt32(self.frame.height / 2)
        let pipeOffset = CGFloat(monementAmount) - self.frame.size.height / 4
        coin.size.width = 40
        coin.size.height = 40
        coin.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: coin.size.width - 20, height: coin.size.height - 20))
        coin.physicsBody?.restitution = 0
        coin.position = CGPoint(x: self.size.width + 50, y: 0 + coinTexture.size().height + 90 + pipeOffset)
        
        let moveCoin = SKAction.moveBy(x: -self.frame.size.width * 2, y: 0, duration: 7)
        let removeAction = SKAction.removeFromParent()
        let coinMoveBg = SKAction.repeatForever(SKAction.sequence([moveCoin, removeAction]))
        coin.run(coinMoveBg)
        
        coin.physicsBody?.isDynamic = false
        coin.physicsBody?.categoryBitMask = coinGroup
        coin.zPosition = 1
        coinObject.addChild(coin)
        
    }
    
    @objc func redCoinAdd() {
        redCoin = SKSpriteNode(texture: redCoinTexture)
        
        coinTexturesArray = [SKTexture(imageNamed: "Coin0.png"), SKTexture(imageNamed: "Coin1.png"), SKTexture(imageNamed: "Coin2.png"), SKTexture(imageNamed: "Coin3.png")]
        let redCoinAnimation = SKAction.animate(with: coinTexturesArray, timePerFrame: 0.2)
        let redCoinHero = SKAction.repeatForever(redCoinAnimation)
        redCoin.run(redCoinHero)
        
        let monementAmount = arc4random() % UInt32(self.frame.height / 2)
        let pipeOffset = CGFloat(monementAmount) - self.frame.size.height / 4
        redCoin.size.width = 40
        redCoin.size.height = 40
        redCoin.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: redCoin.size.width - 10, height: redCoin.size.height - 10))
        redCoin.physicsBody?.restitution = 0
        redCoin.position = CGPoint(x: self.size.width + 50, y: 0 + coinTexture.size().height + 90 + pipeOffset)
        
        let moveCoin = SKAction.moveBy(x: -self.frame.size.width * 2, y: 0, duration: 5)
        let removeAction = SKAction.removeFromParent()
        let coinMoveBg = SKAction.repeatForever(SKAction.sequence([moveCoin, removeAction]))
        redCoin.run(coinMoveBg)
        animations.scaleZdirection(sprite: redCoin)
        animations.redColorAnimation(sprite: redCoin, animDuration: 0.5)
        
        redCoin.setScale(1)
        redCoin.physicsBody?.isDynamic = false
        redCoin.physicsBody?.categoryBitMask = redCoinGroup
        redCoin.zPosition = 1
        redCoinObject.addChild(redCoin)
    }
    
    @objc func addElectricGate() {
        if sound == true {
            run(electricGateCreate)
        }
     electricGate = SKSpriteNode(texture: electricGateTex)
        
        electricGateTexsturesArray = [SKTexture(imageNamed: "ElectricGate01.png"), SKTexture(imageNamed: "ElectricGate02.png"), SKTexture(imageNamed: "ElectricGate03.png"), SKTexture(imageNamed: "ElectricGate04.png")]
        let electricGateAnimation = SKAction.animate(with: electricGateTexsturesArray, timePerFrame: 0.1)
        let electricGateAnimationForever = SKAction.repeatForever(electricGateAnimation)
        electricGate.run(electricGateAnimationForever)
        
        let randomPosition = arc4random() % 2
        let monementAmount = arc4random() % UInt32(self.frame.height / 5)
        let pipeOffset =  self.frame.size.height / 4 + 30 - CGFloat(monementAmount)
        
        
        if randomPosition == 0 {
            electricGate.position = CGPoint(x: self.size.width + 50, y: 0 + electricGateTex.size().height/2 + 90 + pipeOffset)
            electricGate.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: electricGate.size.width - 40, height: electricGate.size.height - 20))
        } else {
             electricGate.position = CGPoint(x: self.size.width + 50, y: self.frame.size.height - electricGateTex.size().height/2 - 90 - pipeOffset)
            electricGate.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: electricGate.size.width - 40, height: electricGate.size.height - 20))
        }
        // Rotate
        electricGate.run(SKAction.repeatForever(SKAction.sequence([SKAction.run({
            self.electricGate.run(SKAction.rotate(byAngle: CGFloat(Double.pi / 2), duration: 0.5))
        }), SKAction.wait(forDuration: 10.0)])))
     
        // Move to hero
        let moveAction = SKAction.moveBy(x: -self.frame.width - 300, y: 0, duration: 6)
        electricGate.run(moveAction)
        
        // Scale
        var scaleValue: CGFloat = 0.3
        
        let scaleRandom = arc4random() % UInt32(5)
        if scaleRandom == 1 {scaleValue = 0.9}
        else if scaleRandom == 2 { scaleValue = 0.6}
        else if scaleRandom == 3 { scaleValue = 0.8}
        else if scaleRandom == 4 { scaleValue = 0.7}
        else if scaleRandom == 0 { scaleValue = 1.0}
        
        electricGate.setScale(scaleValue)
        
        let movementRandom = arc4random() % 9
        if movementRandom == 0 {
            moveElectricGateY = SKAction.moveTo(y: self.frame.height / 2 + 220, duration: 4)
        } else if movementRandom == 1 {
            moveElectricGateY = SKAction.moveTo(y: self.frame.height / 2 - 220, duration: 5)
        } else if movementRandom == 2 {
            moveElectricGateY = SKAction.moveTo(y: self.frame.height / 2 - 150, duration: 4)
        } else if movementRandom == 3 {
            moveElectricGateY = SKAction.moveTo(y: self.frame.height / 2 + 150, duration: 5)
        } else if movementRandom == 4 {
            moveElectricGateY = SKAction.moveTo(y: self.frame.height / 2 + 50, duration: 4)
        } else if movementRandom == 5 {
            moveElectricGateY = SKAction.moveTo(y: self.frame.height / 2 - 50, duration: 5)
        } else {
            moveElectricGateY = SKAction.moveTo(y: self.frame.height / 2, duration: 4)
        }
        electricGate.run(moveElectricGateY)
        
        electricGate.physicsBody?.restitution = 0
        electricGate.physicsBody?.isDynamic = false
        electricGate.physicsBody?.categoryBitMask = objectGroup
        electricGate.zPosition = 1
        movingObject.addChild(electricGate)
    }
    
    @objc func AddMine() {
        mine = SKSpriteNode(texture: mineTexture1)
        let minesRandom = arc4random() % 2
        if minesRandom == 0 {
            mine = SKSpriteNode(texture: mineTexture1)
        } else {
            mine = SKSpriteNode(texture: mineTexture2)
        }
        mine.size.width = 70
        mine.size.height = 62
        mine.position = CGPoint(x: self.frame.size.width + 150, y: self.frame.height / 4 - self.frame.size.height / 24)
        
        let moveMineX = SKAction.moveTo(x: -self.frame.size.width / 4, duration: 4)
        mine.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: mine.size.width - 40, height: mine.size.height - 30))
        mine.physicsBody?.categoryBitMask = objectGroup
        mine.physicsBody?.isDynamic = false
        
        let removeAction = SKAction.removeFromParent()
        let mineMoveBGForever = SKAction.repeatForever(SKAction.sequence([moveMineX, removeAction]))
        
        animations.rotateAnimationMine(sprite: mine, animDuration: 0.2)
        
        mine.run(mineMoveBGForever)
        mine.zPosition = 1
        movingObject.addChild(mine)
    }
    
    func addShield() {
        shield = SKSpriteNode(texture: shieldTexture)
        if sound == true {
            run(shieldOnPreload)
        }
        shield.zPosition = 1
        shieldObject.addChild(shield)
    }
    
    @objc func addShieldItem() {
        shieldItem = SKSpriteNode(texture: shieldItemTexture)
        
        let movementAmount = arc4random() % UInt32(self.frame.size.height / 2)
        let pipeOffset = CGFloat(movementAmount) - self.frame.size.height / 4
        
        shieldItem.position = CGPoint(x: self.size.width + 50, y: 0 + shieldItemTexture.size().height + self.size.height / 2 + pipeOffset)
        shieldItem.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: shieldItem.size.width - 20, height: shieldItem.size.height - 20))
        
        shieldItem.physicsBody?.restitution = 0
        
        let moveShield = SKAction.moveBy(x: -self.frame.width * 2, y: 0, duration: 5)
        let removeAction = SKAction.removeFromParent()
        let shieldItemMoveBG = SKAction.repeatForever(SKAction.sequence([moveShield, removeAction]))
        shieldItem.run(shieldItemMoveBG)
        animations.scaleZdirection(sprite: shieldItem)
        shieldItem.setScale(1.1)
        shieldItem.size.width = 60
        shieldItem.size.height = 60
        
        shieldItem.physicsBody?.isDynamic = false
        shieldItem.physicsBody?.categoryBitMask = shieldGroup
        shieldItem.zPosition = 1
        shieldItemObject.addChild(shieldItem)
    }
    
    func showTapToPlay() {
        tabToPlayLabel.text = "Tap to Fly!"
        tabToPlayLabel.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        tabToPlayLabel.fontSize = 50
        tabToPlayLabel.fontColor = UIColor.white
        tabToPlayLabel.fontName = "Chalkduster"
        tabToPlayLabel.zPosition = 1
        self.addChild(tabToPlayLabel)
    }
    
    func showScore() {
        scoreLabel.fontName = "Chalkduster"
        scoreLabel.text = "0"
        scoreLabel.position = CGPoint(x: self.frame.midX, y: self.frame.maxY - 200)
        scoreLabel.fontSize = 60
        scoreLabel.fontColor = UIColor.white
        scoreLabel.zPosition = 1
        self.addChild(scoreLabel)
    }
    
    func showHightScore() {
        highScoreLabel = SKLabelNode()
        highScoreLabel.position = CGPoint(x: self.frame.maxX - 100, y: self.frame.maxY - 210)
        highScoreLabel.fontSize = 50
        highScoreLabel.fontName = "Chalkduster"
        highScoreLabel.fontColor = UIColor.white
        highScoreLabel.isHidden = true
        highScoreLabel.zPosition = 1
        labelObject.addChild(highScoreLabel)
    }
    
    func showHighScoreText() {
        // highScoreTextLabel = SKLabelNode()
        highScoreTextLabel.position = CGPoint(x: self.frame.maxX - 100, y: self.frame.maxY - 150)
        highScoreTextLabel.fontSize = 30
        highScoreTextLabel.fontName = "Chalkduster"
        highScoreTextLabel.fontColor = UIColor.white
        highScoreTextLabel.text = "HighScore"
        highScoreTextLabel.zPosition = 1
        labelObject.addChild(highScoreTextLabel)
    }
    
    func showStage() {
        stageLabel.position = CGPoint(x: self.frame.maxX - 100, y: self.frame.maxY - 140)
        stageLabel.fontSize = 30
        stageLabel.fontName = "Chalkduster"
        stageLabel.fontColor = UIColor.white
        stageLabel.text = "Stage 1"
        stageLabel.zPosition = 1
       self.addChild(stageLabel)
    }
    
    func timerfunc() {
        timerAddCoin.invalidate()
        timerAddRedCoin.invalidate()
        timerElectricGate.invalidate()
        timerAddMine.invalidate()
        timerAddShieldItem.invalidate()
        
        timerAddCoin = Timer.scheduledTimer(timeInterval: 2.63, target: self, selector: #selector(GameScene.addCoin), userInfo: nil, repeats: true)
        timerAddRedCoin = Timer.scheduledTimer(timeInterval: 8.25, target: self, selector: #selector(GameScene.redCoinAdd), userInfo: nil, repeats: true)
        timerElectricGate = Timer.scheduledTimer(timeInterval: 5.25, target: self, selector: #selector(GameScene.addElectricGate), userInfo: nil, repeats: true)
        timerAddMine = Timer.scheduledTimer(timeInterval: 4.245, target: self, selector: #selector(GameScene.AddMine), userInfo: nil, repeats: true)
        timerAddShieldItem = Timer.scheduledTimer(timeInterval: 20.234, target: self, selector: #selector(GameScene.addShieldItem), userInfo: nil, repeats: true)
    }
    
    func stopGameObject() {
        coinObject.speed = 0
        movingObject.speed = 0
        heroObject.speed = 0
        redCoinObject.speed = 0
    }
    
    func reloadGame() {
        if sound == true {
        SKTAudio.sharedInstance().resumeBackgroundMusic()
        }
        coinObject.removeAllChildren()
        redCoinObject.removeAllChildren()
        
        stageLabel.text = "Stage 1"
        gameover = 0
        scene?.isPaused = false
        
        coinObject.speed = 1
        heroObject.speed = 1
        movingObject.speed = 1
        self.speed = 1
        
        if labelObject.children.count != 0 {
            labelObject.removeAllChildren()
        }
        
        createGround()
        createSky()
        createHero()
        createHeroEmitter()
        
        score = 0
        scoreLabel.text = "0"
        stageLabel.isHidden = false
        highScoreTextLabel.isHidden = true
        showHightScore()
        
        timerAddCoin.invalidate()
        timerAddRedCoin.invalidate()
        timerElectricGate.invalidate()
        timerAddMine.invalidate()
        timerAddShieldItem.invalidate()
        
        timerfunc()
        
    }
    
    override func didFinishUpdate() {
        heroEmiter1.position = hero.position - CGPoint(x: 30, y: 5)
        shield.position = hero.position + CGPoint(x: 0, y: 0)
    }
    
}
