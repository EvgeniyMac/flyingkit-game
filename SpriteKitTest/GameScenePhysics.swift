//
//  GameScenePhysics.swift
//  SpriteKitTest
//
//  Created by Evgeniy Suprun on 18.03.2018.
//  Copyright © 2018 Evgeniy Suprun. All rights reserved.
//

import Foundation
import SpriteKit

extension GameScene {
    func didBegin(_ contact: SKPhysicsContact) {
        
        let objectNode = contact.bodyA.categoryBitMask == objectGroup ? contact.bodyA.node : contact.bodyB.node
        if score > highScore {
            highScore = score
        }
        UserDefaults.standard.set(highScore, forKey: "highScore")
        
        if contact.bodyA.categoryBitMask == objectGroup || contact.bodyB.categoryBitMask == objectGroup {
            hero.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            
            
            
            if shieldBool == false {
                animations.shakeAndFlashAnimation(view: self.view!)
                
                if sound == true {
                    run(electricHeroDead)
                }
                hero.physicsBody?.allowsRotation = false
                
                heroEmiterObject.removeAllChildren()
                coinObject.removeAllChildren()
                redCoinObject.removeAllChildren()
                groundObject.removeAllChildren()
                movingObject.removeAllChildren()
                shieldItemObject.removeAllChildren()
                shieldObject.removeAllChildren()
                
                stopGameObject()
                
                timerAddCoin.invalidate()
                timerAddRedCoin.invalidate()
                timerElectricGate.invalidate()
                timerAddMine.invalidate()
                timerAddShieldItem.invalidate()
                
                heroDeathTextureArray = [SKTexture(imageNamed: "Dead0.png"), SKTexture(imageNamed: "Dead1.png"), SKTexture(imageNamed: "Dead2.png"), SKTexture(imageNamed: "Dead3.png"), SKTexture(imageNamed: "Dead4.png"), SKTexture(imageNamed: "Dead5.png"),SKTexture(imageNamed: "Dead6.png")]
                let heroDeadAnimation = SKAction.animate(with: heroDeathTextureArray, timePerFrame: 5.2)
                hero.run(heroDeadAnimation)
                
                showHightScore()
                gameover = 1
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.scene?.isPaused = true
                    self.heroObject.removeAllChildren()
                    self.showHighScoreText()
                    
                    self.gameViewControllerBridge.reloadGameBtn.isHidden = false
                    self.stageLabel.isHidden = true
                    
                    if self.score > self.highScore {
                        self.highScore = self.score
                    }
                    
                    self.highScoreLabel.isHidden = false
                    self.highScoreTextLabel.isHidden = false
                    self.highScoreLabel.text = "\(self.highScore)"
                })
                
                SKTAudio.sharedInstance().pauseBackgroundMusic()
            } else {
                objectNode?.removeFromParent()
                shieldObject.removeAllChildren()
                shieldBool = false
                if sound == true {run(shieldOffPreload)}
            }
            
            
        }
        if contact.bodyA.categoryBitMask == shieldGroup || contact.bodyB.categoryBitMask == shieldGroup {
             let shieldNode = contact.bodyA.categoryBitMask == shieldGroup ? contact.bodyA.node : contact.bodyB.node
            if shieldBool == false {
                if sound == true {run(pickCoinPreload)}
                shieldNode?.removeFromParent()
                addShield()
                shieldBool = true
            }
        }
        if contact.bodyA.categoryBitMask == groundGroup || contact.bodyB.categoryBitMask == groundGroup {
            if gameover == 0 {
                heroEmiter1.isHidden = true
                heroRunTexturesArray = [SKTexture(imageNamed: "Run0.png"), SKTexture(imageNamed: "Run1.png"), SKTexture(imageNamed: "Run2.png"), SKTexture(imageNamed: "Run3.png"), SKTexture(imageNamed: "Run4.png"), SKTexture(imageNamed: "Run5.png"),SKTexture(imageNamed: "Run6.png")]
                let heroRunAnimation = SKAction.animate(with: heroRunTexturesArray, timePerFrame: 0.1)
                let heroRun = SKAction.repeatForever(heroRunAnimation)
                hero.run(heroRun)
            }
        }
        if contact.bodyA.categoryBitMask == coinGroup || contact.bodyB.categoryBitMask == coinGroup {
            let coinNode = contact.bodyA.categoryBitMask == coinGroup ? contact.bodyA.node : contact.bodyB.node
            if sound == true {
                run(pickCoinPreload)
                
                score = score + 1
                scoreLabel.text = ("\(score)")
            }
            coinNode?.removeFromParent()
        }
        if contact.bodyA.categoryBitMask == redCoinGroup || contact.bodyB.categoryBitMask == redCoinGroup {
            let redCoinNode = contact.bodyA.categoryBitMask == redCoinGroup ? contact.bodyA.node : contact.bodyB.node
            if sound == true {
                run(pickCoinPreload)
                score = score + 5
                scoreLabel.text = ("\(score)")
            }
            redCoinNode?.removeFromParent()
            
        }
    }
}

