//
//  GameSceneTouches.swift
//  SpriteKitTest
//
//  Created by Evgeniy Suprun on 18.03.2018.
//  Copyright © 2018 Evgeniy Suprun. All rights reserved.
//

import Foundation
import SpriteKit

extension GameScene {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if gameover == 0 {
            heroEmiter1.isHidden = false
            
            if tabToPlayLabel.isHidden == false {
                tabToPlayLabel.isHidden = true
            }
            
            if gameover == 0 {
                hero.physicsBody?.velocity = CGVector.zero
                hero.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 180))
                
                heroFlyTexturesArray = [SKTexture(imageNamed: "Fly0.png"), SKTexture(imageNamed: "Fly1.png"), SKTexture(imageNamed: "Fly2.png"), SKTexture(imageNamed: "Fly3.png"), SKTexture(imageNamed: "Fly4.png")]
                let heroFlyAnimation = SKAction.animate(with: heroFlyTexturesArray, timePerFrame: 0.1)
                let flyHero = SKAction.repeatForever(heroFlyAnimation)
                hero.run(flyHero)
            }
        }
    }
}
