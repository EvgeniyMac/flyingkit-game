//
//  Utils.swift
//  SpriteKitTest
//
//  Created by Evgeniy Suprun on 18.03.2018.
//  Copyright © 2018 Evgeniy Suprun. All rights reserved.
//

import Foundation
import CoreGraphics

func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}


