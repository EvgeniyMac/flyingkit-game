//
//  GameViewController.swift
//  SpriteKitTest
//
//  Created by Evgeniy Suprun on 18.03.2018.
//  Copyright © 2018 Evgeniy Suprun. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    
    var scene =  GameScene(size: CGSize(width: 1024, height: 768))
    let textureAtlas = SKTextureAtlas(named: "scene.atlas")
    
    @IBOutlet weak var reloadGameBtn: UIButton!
    @IBOutlet weak var loadingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.isHidden = false
        SwiftSpinner.show(title: "Loading...", animated: true)
        reloadGameBtn.isHidden = true
            let view = self.view as! SKView
        view.ignoresSiblingOrder = true
        
        //view.showsPhysics = true
        scene.scaleMode = .aspectFill
        scene.gameViewControllerBridge = self
        
        //TODO: Break for this block
        textureAtlas.preload {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.loadingView.isHidden = true
            SwiftSpinner.hide()
        SKTAudio.sharedInstance().playBackgroundMusic(filename: "music.mp3")
            view.presentScene(self.scene)
            }
        }
        
    }
    
    @IBAction func reloadGameButton(sender: UIButton) {
        SKTAudio.sharedInstance().playSoundEffect(filename: "button_press.wav")
        scene.reloadGame()
        scene.gameViewControllerBridge = self
        reloadGameBtn.isHidden = true
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
