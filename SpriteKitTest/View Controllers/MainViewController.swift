//
//  MainViewController.swift
//  SpriteKitTest
//
//  Created by Evgeniy Suprun on 15.04.2018.
//  Copyright © 2018 Evgeniy Suprun. All rights reserved.
//

import UIKit
import SpriteKit

class MainViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func startGame(sender: UIButton) {
    SKTAudio.sharedInstance().playSoundEffect(filename: "button_press.wav")
        if let storyboard = storyboard {
            let gameViewController = storyboard.instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
            navigationController?.pushViewController(gameViewController, animated: true)
        }
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
